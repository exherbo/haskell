# Copyright 2012 NAKAMURA Yoshitaka
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Happy Haskell Programming"
DESCRIPTION="
ghc-mod is a backend program to enrich Haskell programming in editors. It
strives to offer most of the features one has come to expect from modern IDEs
in any editor. ghc-mod provides a library for other haskell programs to use as
well as a standalone program for easy editor integration.
"
HOMEPAGE="http://www.mew.org/~kazu/proj/ghc-mod/"

LICENCES="AGPL-3"
SLOT="0"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/binary[>=0.5.1.0&<0.9]
        dev-haskell/bytestring[<0.11]
        dev-haskell/cabal-helper[>=0.6.3.0&<0.8]
        dev-haskell/containers[<0.6]
        dev-haskell/deepseq[<1.5]
        dev-haskell/directory[<1.3]
        dev-haskell/djinn-ghc[>=0.0.2.2&<0.1]
        dev-haskell/extra[=1.4*]
        dev-haskell/fclabels[=2.0*]
        dev-haskell/filepath[<1.5]
        dev-haskell/ghc-paths[<0.2]
        dev-haskell/ghc-syb-utils[<0.3]
        dev-haskell/haskell-src-exts[<1.18]
        dev-haskell/hlint[>=1.8.61&<1.10]
        dev-haskell/monad-control[>=1&<1.1]
        dev-haskell/monad-journal[>=0.4&<0.8]
        dev-haskell/mtl[>=2.0&<2.3]
        dev-haskell/old-time[<1.2]
        dev-haskell/pipes[>=4.1&<4.3]
        dev-haskell/pretty[<1.2]
        dev-haskell/process[<1.5]
        dev-haskell/safe[~>0.3.9]
        dev-haskell/split[<0.3]
        dev-haskell/syb[<0.7]
        dev-haskell/temporary[<1.3]
        dev-haskell/text[<1.3]
        dev-haskell/time[<1.7]
        dev-haskell/transformers[<0.6]
        dev-haskell/transformers-base[<0.5]
    ")
    $(haskell_test_dependencies "
        dev-haskell/doctest[>=0.9.3]
        dev-haskell/hspec[>=2.0.0]
    ")
    $(haskell_bin_dependencies "
        dev-haskell/binary[>=0.5.1.0&<0.9]
        dev-haskell/deepseq[<1.5]
        dev-haskell/directory[<1.3]
        dev-haskell/fclabels[=2.0*]
        dev-haskell/filepath[<1.5]
        dev-haskell/monad-control[=1.0*]
        dev-haskell/mtl[>=2.0&<2.3]
        dev-haskell/old-time[<1.2]
        dev-haskell/optparse-applicative[>=0.11.0&<0.13.0]
        dev-haskell/pretty[<1.2]
        dev-haskell/process[<1.5]
        dev-haskell/split[<0.3]
        dev-haskell/time[<1.7]
    ")
"

# Test phase fails at "spec"
RESTRICT=test
