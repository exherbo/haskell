# Copyright 2014 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2017 Markus Rothe <mrothe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Type classes for mapping, folding, and traversing monomorphic containers"
DESCRIPTION="
Monomorphic variants of the Functor, Foldable, and Traversable typeclasses. If you understand
Haskell's basic typeclasses, you understand mono-traversable. In addition to what you are used to,
it adds on an IsSequence typeclass and has code for marking data structures as non-empty.
"
HOMEPAGE="https://github.com/snoyberg/mono-traversable"

LICENCES="MIT"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/bytestring[>=0.9]
        dev-haskell/containers[>=0.4]
        dev-haskell/hashable
        dev-haskell/split[>=0.2]
        dev-haskell/text[>=0.11]
        dev-haskell/transformers[>=0.3]
        dev-haskell/unordered-containers[>=0.2]
        dev-haskell/vector[>=0.10]
        dev-haskell/vector-algorithms[>=0.6]
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit
        dev-haskell/QuickCheck
        dev-haskell/bytestring
        dev-haskell/containers
        dev-haskell/foldl
        dev-haskell/hspec
        dev-haskell/semigroups
        dev-haskell/text
        dev-haskell/transformers
        dev-haskell/unordered-containers
        dev-haskell/vector
    ")
"

