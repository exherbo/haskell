# Copyright 2013 Alex Elsayed
# Copyright 2017 Markus Rothe <mrothe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Generic programming library for generalised deriving."
DESCRIPTION="
This package provides functionality for generalising the deriving mechanism
in Haskell to arbitrary classes. It was first described in the paper:

*  A generic deriving mechanism for Haskell.
Jose Pedro Magalhaes, Atze Dijkstra, Johan Jeuring, and Andres Loeh.
Haskell'10.

The current implementation integrates with the new GHC Generics. See
http://www.haskell.org/haskellwiki/GHC.Generics for more information.
Template Haskell code is provided for supporting older GHCs.
"
HOMEPAGE="https://github.com/dreixel/generic-deriving"

LICENCES="BSD-3"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/containers[>=0.1&<0.6]
        dev-haskell/template-haskell[>=2.4&<2.12]
    ")
    $(haskell_test_dependencies "
        dev-haskell/hspec[=2*]
        dev-haskell/template-haskell[>=2.4&<2.12]
    ")
"

