# Copyright 2010, 2011, 2012 Markus Rothe
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Provides a number of common functions and types useful in statistics"
DESCRIPTION="
This library provides a number of common functions and types useful in statistics. Our focus is on
high performance, numerical robustness, and use of good algorithms. Where possible, we provide
references to the statistical literature.

The library's facilities can be divided into three broad categories:

Working with widely used discrete and continuous probability distributions. (There are dozens of
exotic distributions in use; we focus on the most common.)

Computing with sample data: quantile estimation, kernel density estimation, bootstrap methods, and
autocorrelation analysis.

Random variate generation under several different distributions.
"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

# Two issues with tests as it currently stands:
#   * Unicode character in test output that LANG=C doesn't like
#   * A certain sub-set of tests fail (when run manually).
# https://github.com/bos/statistics/issues/42
RESTRICT="test"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/aeson[>=0.6.0.0]
        dev-haskell/binary[>=0.5.1.0]
        dev-haskell/deepseq[>=1.1.0.2]
        dev-haskell/erf
        dev-haskell/math-functions[>=0.1.5.2]
        dev-haskell/monad-par[>=0.3.4]
        dev-haskell/mwc-random[>=0.13.0.0]
        dev-haskell/primitive[>=0.3]
        dev-haskell/vector[>=0.10]
        dev-haskell/vector-algorithms[>=0.4]
        dev-haskell/vector-binary-instances[>=0.2.1]
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit
        dev-haskell/QuickCheck[>=2.7.5]
        dev-haskell/binary
        dev-haskell/erf
        dev-haskell/ieee754[>=0.7.3]
        dev-haskell/math-functions
        dev-haskell/mwc-random
        dev-haskell/primitive
        dev-haskell/test-framework
        dev-haskell/test-framework-hunit
        dev-haskell/test-framework-quickcheck2
        dev-haskell/vector
        dev-haskell/vector-algorithms
    ")
"

