# Copyright 2010, 2011, 2017 Markus Rothe <mrothe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Interfacing with RSS (v 0.9x, 2.x, 1.0) + Atom feeds."
DESCRIPTION="
Interfacing with RSS (v 0.9x, 2.x, 1.0) + Atom feeds.

To help working with the multiple feed formats we've
ended up with, this set of modules provides parsers,
pretty printers and some utility code for querying
and just generally working with a concrete representation
of feeds in Haskell.

See here for an example of how to create an Atom feed:
https://github.com/bergmark/feed/blob/master/tests/Example/CreateAtom.hs

For basic reading and editing of feeds, consult
the documentation of the Text.Feed.* hierarchy.
"
HOMEPAGE="https://github.com/bergmark/feed"

LICENCES="BSD-3"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/old-locale[=1.0*]
        dev-haskell/old-time[>=1&<1.2]
        dev-haskell/time[<1.7]
        dev-haskell/time-locale-compat[=0.1*]
        dev-haskell/utf8-string[<1.1]
        dev-haskell/xml[>=1.2.6&<1.3.15]
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit[>=1.2&<1.6]
        dev-haskell/old-locale[=1.0*]
        dev-haskell/old-time[>=1&<1.2]
        dev-haskell/test-framework[=0.8*]
        dev-haskell/test-framework-hunit[=0.3*]
        dev-haskell/time[<1.7]
        dev-haskell/time-locale-compat[=0.1*]
        dev-haskell/utf8-string[<1.1]
        dev-haskell/xml[>=1.2.6&<1.4]
    ")
"

RESTRICT="test"

