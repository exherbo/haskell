# Copyright 2013 Alex Elsayed
# Distributed under the terms of the GNU General Public License v2

require hackage

SUMMARY="Lenses, Folds and Traversals"
DESCRIPTION="
This package comes \"Batteries Included\" with many useful lenses for the types
commonly used from the Haskell Platform, and with tools for automatically
generating lenses and isomorphisms for user-supplied data types.

The combinators in @Control.Lens@ provide a highly generic toolbox for composing
families of getters, folds, isomorphisms, traversals, setters and lenses and their
indexed variants.
"

LICENCES="BSD-3"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/array[>=0.3.0.2&<0.6]
        dev-haskell/base-orphans[~>0.3]
        dev-haskell/bifunctors[=5*]
        dev-haskell/bytestring[>=0.9.1.10&<0.11]
        dev-haskell/comonad[>=4&<6]
        dev-haskell/containers[>=0.4.0&<0.6]
        dev-haskell/contravariant[~>1.3]
        dev-haskell/distributive[~>0.3]
        dev-haskell/exceptions[>=0.1.1&<1]
        dev-haskell/filepath[>=1.2.0.0&<1.5]
        dev-haskell/free[=4*]
        dev-haskell/hashable[>=1.1.2.3&<1.3]
        dev-haskell/kan-extensions[>=5&<6]
        dev-haskell/mtl[>=2.0.1&<2.3]
        dev-haskell/parallel[>=3.1.0.1&<3.3]
        dev-haskell/profunctors[=5*]
        dev-haskell/reflection[~>2.1]
        dev-haskell/semigroupoids[=5*]
        dev-haskell/semigroups[>=0.8.4&<1]
        dev-haskell/tagged[>=0.4.4&<1]
        dev-haskell/template-haskell[>=2.4&<2.12]
        dev-haskell/text[>=0.11&<1.3]
        dev-haskell/transformers[>=0.2&<0.6]
        dev-haskell/transformers-compat[~>0.4]
        dev-haskell/unordered-containers[~>0.2.4]
        dev-haskell/vector[>=0.9&<0.12]
        dev-haskell/void[~>0.5]
    ")
    $(haskell_test_dependencies "
        dev-haskell/HUnit[>=1.2]
        dev-haskell/QuickCheck[>=2.4]
        dev-haskell/bytestring
        dev-haskell/containers
        dev-haskell/deepseq
        dev-haskell/directory[>=1.0]
        dev-haskell/doctest[>=0.9.1]
        dev-haskell/filepath
        dev-haskell/generic-deriving
        dev-haskell/hlint[>=1.7]
        dev-haskell/mtl
        dev-haskell/nats
        dev-haskell/parallel
        dev-haskell/semigroups[>=0.9]
        dev-haskell/simple-reflect[>=0.3.1]
        dev-haskell/test-framework[>=0.6]
        dev-haskell/test-framework-hunit[>=0.2]
        dev-haskell/test-framework-quickcheck2[>=0.2]
        dev-haskell/test-framework-th[>=0.2]
        dev-haskell/text
        dev-haskell/transformers
        dev-haskell/unordered-containers
        dev-haskell/vector
    ")
"

# Tests fail to build
RESTRICT="test"

